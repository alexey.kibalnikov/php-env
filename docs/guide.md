#### Default Admin
+ login: <b>admin@mail.com</b>
+ password: <b>V42m^#JMzaXB@%96</b>

### SQL connection
+ host: <b>localhost</b>
+ port: <b>5432</b>
+ user: <b>api</b>
+ password: <b>vW4%yyN9NXtfk@#z</b>
+ database: <b>consolidator</b>

#### Проверить подключен ли xDebug
```
docker-compose exec php php -v
```

#### Composer (установить зависимости)
```
docker-compose exec php sh -c "composer install --no-interaction --no-scripts"
```

#### Composer добавление пакетов (два примера)
```
docker-compose exec php sh -c "composer require logger/logger --no-interaction --no-scripts"
docker-compose exec php sh -c "composer require maker --dev"
```

#### Генерация Entity (Symfony's console)
```
docker-compose exec php sh -c "php bin/console make:entity"
```

#### Генерация файла миграции (Symfony's console)
```
docker-compose exec php sh -c "php bin/console make:migration"
```

#### Запуск миграции (Symfony's console)
```
docker-compose exec php sh -c "php bin/console doctrine:migrations:migrate --no-interaction"

docker-compose exec php sh -c "php bin/console doctrine:migrations:status"
docker-compose exec php sh -c "php bin/console doctrine:migrations:migrate prev"
docker-compose exec php sh -c "php bin/console doctrine:migrations:migrate next"

docker-compose exec php sh -c "php bin/console doctrine:migrations:execute 'DoctrineMigrations\Version20210211060921' --down"
docker-compose exec php sh -c "php bin/console doctrine:migrations:execute 'DoctrineMigrations\Version20210211060921' --up"
```

### Fixtures заливаем не удаляя данных (Symfony's console)
```
docker-compose exec php sh -c "php bin/console doctrine:fixtures:load --append"
```

#### CodeSniffer
```
docker-compose exec php sh -c "composer cs"
```

#### PHPUnit
```
docker-compose exec php sh -c "composer test:run"
```

#### Cache (Symfony's console)
```
docker-compose exec php sh -c "php bin/console cache:clear"
```

#### Посмотреть все роуты (Symfony's console)
```
docker-compose exec php sh -c "php bin/console debug:router"
```

#### Посмотреть все валидации (Symfony's console)
```
docker-compose exec php sh -c "php bin/console debug:validator src/Entity"
docker-compose exec php sh -c "php bin/console debug:validator src/Dto"
```

#### Посмотреть контейнер (Symfony's console)
```
docker-compose exec php sh -c "php bin/console debug:container"
```

#### Интерфейсы для использования через контейнер (Symfony's console)
```
docker-compose exec php sh -c "php bin/console debug:autowiring --all"
```

#### Генерация класса контроллера (Symfony's console)
```
docker-compose exec php sh -c "php bin/console make:controller HomeController"
```

#### Создание схемы БД (Symfony's console)
```
docker-compose exec php sh -c "php bin/console doctrine:database:create"
```

#### ORM создание реляционных таблиц из объектов без миграции (Symfony's console)
```
docker-compose exec php sh -c "php bin/console doctrine:schema:update --force"
```

#### Очистить Log-файл (Bash console)
```
echo -n > dev.log
```