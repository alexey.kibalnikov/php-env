#!/bin/bash

# создем файлы
## скопировать .env.example -> .env
cp .env.example .env
mkdir sites

# TODO: создать файлы если они нужны
#  tgs.txt
touch container/web_backend/log/xdebug.log

# docker and docker-compose install
sudo apt-get remove docker docker-engine docker.io
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable" -y
sudo apt-get update -y
sudo apt-get install docker-ce docker-compose -y

# install additional program
apt-get update
apt-get upgrade -y
apt-get install zsh vim tmux rsync curl git zip unzip -y
rsync -av dotfiles/ ~/
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
sed -i 's/robbyrussell/darkblood/g' ~/.zshrc
