DOCKER_COMPOSE_NAME_PHP=php

first:
	cp ./.env-example ./.env \
	&& make up \
	&& make install \
	&& make migr

# Docker-compose
up:
	docker-compose up -d

down:
	docker-compose down

# Composer
install:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "composer install --no-interaction --no-scripts"

update:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "composer update"

recipes:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "composer recipes"

recipes-install:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "composer recipes:install vendor/package --force -v"

# CodeSniffer
cs:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "composer cs"

# (Symfony's CLI) Migrations
migr-gen:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console make:migration"

migr:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console doctrine:migrations:migrate --no-interaction"

migr-status:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console doctrine:migrations:status"

migr-prev:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console doctrine:migrations:prev"

migr-next:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console doctrine:migrations:next"

# (Symfony's CLI) Cache
cache-clear:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console cache:clear --no-warmup --env=dev"

# (Symfony's CLI) Router show ALL
router:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console debug:router"

# (Symfony's CLI) Container show ALL
container:
	docker-compose exec ${DOCKER_COMPOSE_NAME_PHP} sh -c "php bin/console debug:container"

# (Bash) Log clear
clear-log:
	echo -n > ./var/log/dev.log