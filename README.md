## Документация к API

> #### Рекомендации по безопасности [docs/security.md](/docs/security.md)
>
> #### Для BACK разработчиков [docs/guide.md](/docs/guide.md)
> 
> #### Для FRONT разработчиков [Postman](https://documenter.getpostman.com/view/12567244/TW6xmSgg)
> можно произвести импорт из директории **docs/postman** (ENV + collection)

## Установка

#### #0 Создать и выдать права на директории (linux)
    ./../uploads - на уровне хранения контейнера

В ней должны содержаться:

    uploads/passport
    uploads/batch

#### #1 Собрать контейнер (docker)
```
docker-compose up --build -d
```

#### #2 Установить зависимости (composer)
```
docker-compose exec php sh -c "composer install --no-interaction --no-scripts"
```
Если **prod**, то запустить еще второй скрипт (помимо предыдущего)
```
docker-compose exec php sh -c "composer dump-autoload --optimize --no-dev --classmap-authoritative --no-interaction --no-scripts"
```
После этого появится vendor/composer/**autoload_classmap.php**

#### #3 Запустить миграцию данных (Symfony's console)
```
docker-compose exec php sh -c "php bin/console doctrine:migrations:migrate --no-interaction"
```

#### #4 Выдать права на директории (linux)
```
sudo chmod 777 -R ./var
```

#### #5 Использовать "env.local"
> + Переименовать файл "env.local-example" в "env.local"
> + Внести в него нужное содержимое **dev/prod** и пр.